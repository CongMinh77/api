<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    protected $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    public function index()
    {
        $tasks = $this->task->all();
        //$tasks = $this->task->paginate(10);
        return view('tasks.index', compact('tasks'));
    }

    public function store(StoreTaskRequest $request)
    {
        $dataResource = $request->all();
        $this->task->create($dataResource);
        return redirect()->route('tasks.index');
    }

    public function create()
    {
        return view('tasks.create');
    }

    public function show($id)
    {
        $task = $this->task->findOrFail($id);
        return view('tasks.show', compact('task'));
    }

    public function update(UpdateTaskRequest $request, $id)
    {
        $task = $this->task->findOrFail($id);
        $dataUpdate = $request->all();

        $task->update($dataUpdate);
        return redirect()->route('tasks.index');
    }

    public function destroy($id)
    {
        $task = $this->task->findOrFail($id);
        $task->delete($id);
        return redirect()->route('tasks.index');
    }
}
