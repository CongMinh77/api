@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card justify-content-center" style="width: 100%;">
            <div class="card-body">
                <h5 class="card-title">{{ $task->id }}</h5>
                <h6 class="card-subtitle mb-2 text-muted">{{ $task->name }}</h6>
                <p class="card-text">{{ $task->content }}</p>
                <p class="card-text">{{ $task->created_at }}</p>
                <p class="card-text">{{ $task->updated_at }}</p>
            </div>
        </div>
    </div>
@endsection
