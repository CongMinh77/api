@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <h2 class="text-center">Create task</h2>
                <form action="{{route('tasks.store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="Name ...">
                        @error('name')
                            <span class="error text-danger">
                                {{$message}}
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="content" placeholder="Content ...">
                    </div>
                    <button class="btn btn-success form-control">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
