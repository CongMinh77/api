@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <table class="table table-striped">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Content</th>
                    <th>Actions</th>
                </tr>
                @foreach($tasks as $task)
                    <tr>
                        <th>{{$task->id}}</th>
                        <th>{{$task->name}}</th>
                        <th>{{$task->content}}</th>
                        <th>
                            <div class="d-flex">
                                <form method="POST" action="{{ route('tasks.destroy', $task->id) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger">Delete</button>
                                </form>
                                <a href="" class="btn btn-warning ml-2">Edit</a>
                                <a href="{{route('tasks.show', $task->id)}}" class="btn btn-outline-info ml-2">...</a>
                            </div>
                        </th>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

@endsection
