<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdatePostTest extends TestCase
{
    /** @test */
    public function user_can_update_post_if_post_exists_and_data_is_valid()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'name'=> $this->faker->name,
            'body' => $this->faker->text
        ];
        //$response = $this->get('/');
        $response = $this->json('PUT', route('posts.update', $post->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(fn(AssertableJson $json)=>
            $json->has('status')
            ->has('message')
            ->has('data', fn(AssertableJson $json)=>
                $json->where('name', $dataUpdate['name'])
                ->where('body', $dataUpdate['body'])
                ->etc()
            )
        );

        $this->assertDatabaseHas('posts',[
            'name' => $dataUpdate['name'],
            'body' => $dataUpdate['body']
        ]);
    }

    /** @test */
    public function user_can_update_post_if_post_exists_and_name_is_null()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'name'=> '',
            'body' => $this->faker->text
        ];
        //$response = $this->get('/');
        $response = $this->json('PUT', route('posts.update', $post->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(fn(AssertableJson $json)=>
        $json->has('errors', fn(AssertableJson $json)=>
            $json->has('name')
                ->etc()
            )
        );
    }
    /** @test */
    public function user_can_update_post_if_post_exists_and_data_have_only_name()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'name'=> $this->faker->name,
        ];
        //$response = $this->get('/');
        $response = $this->json('PUT', route('posts.update', $post->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(fn(AssertableJson $json)=>
        $json->has('status')
            ->has('message')
            ->has('data', fn(AssertableJson $json)=>
                $json->has('name')
                ->has('body')
                ->etc()
            )
            ->etc()
        );
    }
    /** @test */
    public function user_can_update_post_if_post_exists_and_data_is_unvalid()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'name'=> '',
            'body' => ''
        ];
        //$response = $this->get('/');
        $response = $this->json('PUT', route('posts.update', $post->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(fn(AssertableJson $json)=>
        $json->has('errors', fn(AssertableJson $json)=>
        $json->has('name')
            ->etc()
            )
        );
    }
    /** @test */
    public function user_can_update_post_if_post_not_exists_and_data_is_valid()
    {
        $postId = -1;
        $dataUpdate = [
            'name'=> $this->faker->name,
            'body' => $this->faker->text
        ];
        //$response = $this->get('/');
        $response = $this->json('PUT', route('posts.update', $postId), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
