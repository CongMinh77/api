<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeletePostTest extends TestCase
{
    /** @test */
    public function user_can_delete_if_post_exists()
    {
        //$response = $this->get('/');
        $post = Post::factory()->create();
        $postCountBeforeDelete = Post::count();
        $response = $this->json('DELETE', route('posts.destroy', $post->id));
        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(fn(AssertableJson $json)=>
        $json->has('status')
            ->has('message')
            ->has('data', fn(AssertableJson $json)=>
            $json->where('name', $post->name)
                ->where('body', $post->body)
                ->etc()
            )
        );
        $postCountAfterDelete = Post::count();

        $this->assertEquals($postCountBeforeDelete-1, $postCountAfterDelete);
    }

    /** @test */
    public function user_can_delete_if_post_not_exists()
    {
        //$response = $this->get('/');
        $postId = -1;
        $response = $this->json('DELETE', route('posts.destroy', $postId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
