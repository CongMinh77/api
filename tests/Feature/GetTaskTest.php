<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetTaskTest extends TestCase
{
    public function getShowTaskRoute($id){
        return route('tasks.show', $id);
    }
    /** @test */
    public function user_can_get_a_task_by_id_if_task_exists()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getShowTaskRoute($task->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.show');
        $response->assertSee($task->name);
    }
    /** @test */
    public function user_can_not_get_a_task_by_id_if_task_not_exists()
    {
        $taskId = -1;
        $response = $this->get($this->getShowTaskRoute($taskId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
