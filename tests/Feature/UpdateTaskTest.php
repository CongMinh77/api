<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{
    public function getUpdateTaskRoute($id)
    {
        return route('tasks.update', $id);
    }
    /** @test */
    public function authenticated_user_can_update_task_if_task_exists()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->put($this->getUpdateTaskRoute($task->id), $task->toArray());

        $response->assertStatus(302);
    }
}
